package com.stackroute.egmvc.controller;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.stackroute.egmvc.model.Book;
import com.stackroute.egmvc.repo.Bookrepo;

@Controller
public class MyController {
	
	ApplicationContext aptct=new ClassPathXmlApplicationContext("beans.xml");
	Book book=aptct.getBean("book",Book.class);
	Bookrepo bookrep=aptct.getBean("bookrepo",Bookrepo.class);
	
	public MyController()
	{
		
	}
	
	@GetMapping("/")
	public String firstUrl()
	{
		return "index";
	}

	@PostMapping("/store")
	public String storeRec(@ModelAttribute("book") Book bk,ModelMap mp )
	{
		// book.setBookid(id);
		// book.setBookname(name);
		 bookrep.addElement(bk);
		 mp.addAttribute("booklist",bookrep.getBooks());
		return "index";
		
	}
	
	@GetMapping("/deleterec")
	public ModelAndView delRec(@RequestParam("bookid") int bid,ModelMap mp)

	{
		System.out.println(bid);
		bookrep.deleteElement(bid);
		ModelAndView modelview=new ModelAndView("index");
		
		modelview.addObject("booklist",bookrep.getBooks());
		
		//mp.addAttribute("booklist",bookrep.getBooks());
		return modelview;
		
	}
	@GetMapping("/viewall")
	 public String showAll()
	 {
		List<Book> listobj=bookrep.getBooks();
		System.out.println(listobj.size());
		return "index";
	 }
}
