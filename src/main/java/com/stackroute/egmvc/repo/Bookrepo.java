package com.stackroute.egmvc.repo;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import com.stackroute.egmvc.model.Book;

public class Bookrepo {
	
	List<Book> books;
	
	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public Bookrepo()
	{
		books=new ArrayList<Book>();
	}

	public void addElement(Book bk)
	{
		books.add(bk);
	}
	
	public boolean deleteElement(int bookid)
	{
		
		ListIterator lobj=books.listIterator();
		
		while(lobj.hasNext())
		{
			Book bk=(Book)lobj.next();
			
			if(bk.getBookid()==bookid)
				lobj.remove();
		
		}
		
		return true;
	}
	
	
}
